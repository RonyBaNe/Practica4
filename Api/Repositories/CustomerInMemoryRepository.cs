using System;
using System.Collections.Generic;
using System.Linq;
using Api.Services.Interfaces;
using Api.Repositories.Interfaces;
using Entities;
using System.Threading.Tasks;
using Entities.Dto;

namespace Api.Repositories
{
    public class CustomerInMemoryRepository : ICustomerRepository
    {
        private readonly List<Customer> _customers;

        public CustomerInMemoryRepository()
        {
            _customers = new List<Customer>();
        }

        public async Task<List<Customer>> GetAllAsync()
        {
            
            return await Task.FromResult(_customers);
            /*var lst = new List<Customer>();
            lst.Add(new Customer{ Id = 1, Name = "Customer"});

            return await Task.FromResult(lst);;*/
        }
        public async Task<Customer> SaveAsync(Customer customer)
        {
            int id = _customers.Count + 1;
            customer.Id = id;

            _customers.Add(customer);

            return await Task.FromResult(customer);
        }

        public async Task<bool> DeleteAsync(int id)
        {
           return await Task.FromResult(_customers.Remove(_customers.Single(x => x.Id == id)));

        }
        public async Task<Customer> GetCustomerByIdAsync(int id)
        {
            return await Task.FromResult(_customers.FirstOrDefault(x=> x.Id ==id));
        }


        public async Task<Customer> UpdateAsync(Customer customer)
        {
            var index = _customers.FindIndex(x=> x.Id == customer.Id);
            
            if (index != -1)
            {
                _customers[index] = customer;
            }

            return await Task.FromResult(customer);
        }

        public Task<List<CustomerDto>> SearchAsync()
        {
            throw new NotImplementedException();
        }
    }
}