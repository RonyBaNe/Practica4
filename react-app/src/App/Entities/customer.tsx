import IState from "./state";

export default interface ICustomer{
    id:number,
    name:string,
    rfc:string,
    phone:string,
    email:string,
    address:string,
    stateCode:string,
    state:IState,
}