

using Dapper;
using Dapper.Contrib.Extensions;
using Api.DataAccess.Interfaces;
using Api.Repositories.Interfaces;
using Entities;

namespace Api.Repositories;


public class StateRepository : IStateRepository
{
    private readonly IData _data;


    public StateRepository(IData data)
    {
        _data = data;
    }

    public async Task<List<State>> GetByCountryAsync(string countryCode)
    {
        var sql = "SELECT * FROM State WHERE CountryCode = @CountryCode";

        var states = 
        (await _data.DbConnection.QueryAsync<State>(sql, new { CountryCode = countryCode})).ToList();

        return states;
    }

}