using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Entities;

namespace Entities.Dto
{
    public class CustomerDto: Customer
    {
        public State State{ get; set;}
    }
}