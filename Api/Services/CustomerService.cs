using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Api.Services.Interfaces;
using Api.Repositories.Interfaces;
using Entities;
using Entities.Dto;

namespace Api.Services
{
    public class CustomerService: ICustomerService
    {
        private readonly ICustomerRepository _icustomrepository;
        public CustomerService(ICustomerRepository icustomrepository){
            _icustomrepository = icustomrepository;
        }
        public async Task<List<Customer>> GetAllAsync()
        {
            return await _icustomrepository.GetAllAsync();
        }
        public async Task<bool> DeleteAsync(int id)
        {
            return await _icustomrepository.DeleteAsync(id);
        }

        public async Task<Customer> GetCustomerByIdAsync(int id)
        {
            return await _icustomrepository.GetCustomerByIdAsync(id);
        }

        public async Task<Customer> SaveAsync(Customer customer)
        {
            return await _icustomrepository.SaveAsync(customer);
        }

        public async Task<Customer> UpdateAsync(Customer customer)
        {
            return await _icustomrepository.UpdateAsync(customer);
        }

        public async Task<List<CustomerDto>> SearchAsync()
        {
            return await _icustomrepository.SearchAsync();
        }
    }
}