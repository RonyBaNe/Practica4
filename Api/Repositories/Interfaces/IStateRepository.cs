using Entities;

namespace Api.Repositories.Interfaces;

public interface IStateRepository
{
    Task<List<State>> GetByCountryAsync(string countryCode);
}
