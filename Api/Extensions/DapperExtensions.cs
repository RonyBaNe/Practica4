using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Extensions;

public static class DapperExtensions
{
    public static void ConfigureSqlMapperExtensions(string nameSpace)
    {

        Dapper.Contrib.Extensions.SqlMapperExtensions.TableNameMapper = (entityyName) => {
            
            var name = entityyName.ToString();

            if(name.Contains(nameSpace))
            {
                //Regresa la cadena de texto minuscula
                name = name.Replace(nameSpace, string.Empty);
            }

            //name  = ToUpperFirstLetter(name);
            return name;
        };

    }

    /*
    private static string ToUpperFirstLetter(string source) 
    {
        if(string.IsNullEmpty(source))
            return string.Empty;
        

        return char.ToUpper(source[0]) + source.Substring(1);


    }
    
    */


}