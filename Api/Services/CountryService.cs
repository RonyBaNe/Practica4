
using Api.Repositories;
using Api.Repositories.Interfaces;
using Api.Services.Interfaces;
using Entities;

namespace Api.Services;

public class CountryService : ICountryService
{
    private readonly IStateRepository _stateRepository;
    public CountryService(IStateRepository stateRepository)
    {
        _stateRepository = stateRepository;
    }


    public async Task<List<State>> GetStateByCountryCodeAsync(string countryCode)
    {
        return await _stateRepository.GetByCountryAsync(countryCode);
    }
}