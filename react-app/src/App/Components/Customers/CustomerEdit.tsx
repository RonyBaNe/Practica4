import { observer } from 'mobx-react-lite'
import React, { useState } from 'react'

import ICustomer from '../../Entities/customer'
import IState from '../../Entities/state'
import { useStore } from '../../Store/store'


const CustomerEdit = () => {

    const {customerStore} = useStore()
    const {states ,selectedCustomer, saveCustomerEvent, cancelEditEvent} = customerStore

    let defaultState = {
        code: '',
        countryCode: '',
        name: '',
        taxAuthorityCode: '',
    }

    let deafultCustomer = {
        id: 0,
        name: '',
        rfc: '',
        phone: '',
        email: '',
        address: '',
        stateCode: '',
        state: defaultState,
    }

    let customerRef:ICustomer = (selectedCustomer != null)? selectedCustomer: deafultCustomer

    let label = customerRef.id === 0 ? 'New Customer': 'Edit Customer'

    const[customer, setcustomer] = useState<ICustomer>(customerRef)

    const handleInputChange = (event: any) => {
        const{name, value} = event.target
        setcustomer({...customer,[name]:value})
    } 

    const handleCountryStateChange = (event:any) => {
        const {value} = event.target
        setcustomer({...customer, 'stateCode':value})
    }

    return(
        <React.Fragment>
            <h3 className="mt-3 mb-3">{label}</h3>
            <form>
                <div className="mb-3">
                    <label  className="form-label">Name</label>
                    <input onChange={handleInputChange} type="text" className="form-control" id="name" value={customer.name}  name="name" aria-describedby="emailHelp"/>
                </div>
                <div className="mb-3">
                    <label className="form-label">RFC</label>
                    <input onChange={handleInputChange} type="text" className="form-control" value={customer.rfc} id="rfc" name="rfc"/>
                </div>
                <div className="mb-3">
                    <label className="form-label">Phone</label>
                    <input onChange={handleInputChange} type="text" className="form-control" value={customer.phone} id="phone" name="phone"/>
                </div>

                <div className="mb-3">
                    <label className="form-label">Address</label>
                    <input onChange={handleInputChange} type="text" className="form-control" value={customer.address} id="address" name="address"/>
                </div>

                <div className="mb-3">
                    <label className="form-label">Email</label>
                    <input onChange={handleInputChange} type="text" className="form-control" value={customer.email} id="email" name="email"/>
                </div>

                <div className="mb-3">
                    <label className="form-label">State</label>
                    <select id="state" onChange={handleCountryStateChange} value={customer.stateCode} className="form-control">
                        {
                            // eslint-disable-next-line array-callback-return
                            states.map((state: IState) => (
                                <option key={state.code} value={state.code}>
                                    {state.name}
                                </option>
                            ))
                        }
                    </select>
                </div>
            
                <button type="button" onClick={() => saveCustomerEvent(customer)} className="btn btn-primary">Save</button>
                <button type='button' onClick={ () => cancelEditEvent()} className='btn btn-dark m-2'>Cancel</button>
            </form>
        </React.Fragment>
    )
}

export default observer(CustomerEdit);