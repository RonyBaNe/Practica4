using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Entities;
using Api.Responses;
using Api.Services.Interfaces;
using Entities.Dto;

namespace Api.Controllers
{
    [ApiController]
    public class CustomerController : ControllerBase
    {

        private readonly ICustomerService _customerservice;


        public CustomerController(ICustomerService customerService)
        {
            _customerservice = customerService;
    
        }

        [HttpGet]
        [Route("api/[controller]")]
        public async Task<ActionResult<Response<IEnumerable<Customer>>>> GetAll()
        {
        
        var response = new Response<IEnumerable<Customer>>();
        var customer = await _customerservice.GetAllAsync();
        response.Data = customer;

        return Ok(response);
        }

        [HttpGet]
        [Route("api/[controller]/search")]
        public async Task<ActionResult<Response<IEnumerable<CustomerDto>>>> Search()
        {
        
        var response = new Response<IEnumerable<CustomerDto>>();
        var customer = await _customerservice.SearchAsync();
        response.Data = customer;

        return Ok(response);
        }



        [HttpGet]
        [Route("api/[controller]/{id}")]
        public async Task<ActionResult<Response<Customer>>> GetAById(int id)
        {
        
            var response = new Response<Customer>();
            var customer = await _customerservice.GetCustomerByIdAsync(id);
            response.Data = customer;

            if (customer == null)
            {
                response.Message = "Customer not found";
                response.Errors.Add("The customer id provider was not found");

                return NotFound(response);
            }

            response.Data = customer;
            return Ok(response);
        }


        [HttpPost]
        [Route("api/[controller]")]
        public async Task<ActionResult<Response<Customer>>> Save([FromBody] Customer customer)
        {   
            var response = new Response<Customer>();
            customer = await _customerservice.SaveAsync(customer);
            response.Data = customer;

            return Created("api/customer/" + customer.Id, response);
        }



        [HttpPut]
        [Route("api/[controller]")]
        public async Task<ActionResult<Response<Customer>>> Update([FromBody] Customer customer)
        {   
            var response = new Response<Customer>();
            customer = await _customerservice.UpdateAsync(customer);
            response.Data = customer;

            return Ok(response);
        }


        [HttpDelete]
        [Route("api/[controller]/{id}")]
        public async Task<ActionResult<Response<bool>>> Delete(int id)
        {
        
            var response = new Response<bool>();
            response.Data = await _customerservice.DeleteAsync(id);
            
            return Ok(response);
        }
    }
}