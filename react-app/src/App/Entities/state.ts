export default interface IState{
    code:string,
    countryCode:string,
    name:string,
    taxAuthorityCode:string 
}
