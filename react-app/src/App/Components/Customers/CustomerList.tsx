import { observer } from "mobx-react-lite";
import React from "react";
import { Toaster,toast  } from 'react-hot-toast'

import ICustomer from "../../Entities/customer";
import { useStore } from "../../Store/store";



/*interface IProps{
  customers: ICustomer[],
  editCustomerEvent: (customer:ICustomer|null) => void,
}*/


const CustomerList = () => {

  const {customerStore} = useStore()
  const {customers, editCustomerEvent, deleteCustomerEvent} = customerStore

  return (
    <React.Fragment>
      <h3 className="mt-3 mb-3">Customers</h3>

      <div className="row mb-3">
        <div className="col6">
          <input
            type="text"
            className="form-control"
            placeholder="Name or RFC"
            aria-label="First name"
          />
        </div>
      </div>

      <div className="row mb-3">
            <div className="col-md-12 text-right">
                <button onClick={ () => editCustomerEvent(null) } type="button" className="btn btn-success">
                  New
                </button>
            </div>
        </div>

      <table className="table">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Name</th>
            <th scope="col">RFC</th>
            <th scope="col">Email</th>
            <th scope="col">State</th>
            <th scope="col">Actions</th>
          </tr>
        </thead>
        <tbody>
          {
            // eslint-disable-next-line array-callback-return
            customers.map((customer:ICustomer) => (
              <tr key={customer.id}>
                <th scope="row">{customer.id}</th>
                <td>{customer.name}</td>
                <td>{customer.rfc}</td>
                <td>{customer.email}</td>
                <td>{customer.state.name}</td>
                <td>
                  <button onClick={() => editCustomerEvent(customer)} type="button" className="btn btn-primary">
                      Edit
                  </button>
                  &nbsp;&nbsp;
                  <button onClick={() => { toast((t) => (
                    <span>
                      Want to remove this?                      
                      &nbsp;
                      <button onClick={() => deleteCustomerEvent(customer)} type="button" className="btn btn-primary">
                        Delete
                      </button>
                      &nbsp;
                      <button onClick={() => toast.dismiss(t.id)} type="button" className="btn btn-danger">
                        Cancel
                      </button>

                    </span>
                  )); }} type="button" className="btn btn-danger">
                    Delete
                  </button>
                </td>
              </tr>
            ))
          }
          
        </tbody>
      </table>
      <Toaster
      position='top-center'
      reverseOrder={false} />
    </React.Fragment>
    
  );
};

export default observer(CustomerList);
