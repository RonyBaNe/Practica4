import React, { useEffect, useState } from 'react'
import { observer } from 'mobx-react-lite';


import CustomersDashboard from '../Components/Customers/CustomersDashboard';
import api from '../Api/api';
import ICustomer from '../Entities/customer';
import { useStore } from '../Store/store';



const Customers = () => {


    const {customerStore} = useStore()

    useEffect(() => {
        customerStore.loadCustomer()
    }, [])

    

    return(
        <React.Fragment>
            {customerStore.title}
            <button onClick={customerStore.setTitle}>Do It!</button>
            <div>
                <CustomersDashboard/>
            </div>
        </React.Fragment>
    )
}

export default Customers;