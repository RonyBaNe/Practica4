import axios from 'axios';
import {action, makeAutoObservable, makeObservable, observable} from 'mobx'
import api from '../Api/api';
import ICustomer from '../Entities/customer';
import IState from '../Entities/state';


export default class CustomerStore{

    title = 'Hello World'
    editCustomer: Boolean = false
    isListLoaded: Boolean = false
    customers: ICustomer[] = []
    states: IState[] = []
    selectedCustomer: ICustomer|null = null
    errorMessage:String = ''

    


    constructor(){

        makeAutoObservable(this)
      
    }


    setTitle = () => {
        this.title = this.title + '!'
    }


    addError(error:String){
        this.errorMessage = error;
    }


    loadCustomer = async() => {
        try {
            
            const responseCustomer = await api.Customer.search()
            const responseState = await api.Country.listStateByCountry('MX')


            this.isListLoaded = true
            this.customers = responseCustomer.data
            this.states = responseState.data

        } catch (error) {

            this.addError(String(error));
            
            
        }
    }

    deleteCustomerEvent = async (customer:ICustomer) => {
        
        await axios.delete('https://localhost:7181/api/customer/' + customer.id)
        
    }

    editCustomerEvent = (customer: ICustomer|null) => {
        this.editCustomer = true
        this.selectedCustomer = customer
    }


    cancelEditEvent = () => {
        this.editCustomer = false
        this.selectedCustomer = null
    }


    saveCustomerEvent = async(customer:ICustomer) => {
        try{

            if(customer?.id === 0)
            {
                const response = await api.Customer.create(customer);
                this.customers.push(response.data);
            }
            else
            {
                const response = await api.Customer.update(customer);
                let index = this.customers.findIndex( u=> u.id === customer.id);
                this.customers[index] = customer;
            }

            this.editCustomer = false;
            this.selectedCustomer = null;

        }catch(error){
            this.addError(String(error));
        }

    }


}